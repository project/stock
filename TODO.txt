TODO:

2005-02-04:

- Make the attribute for 'change' column to be themable, so theme designers 
  can add a custom up arrow, down arrow, color, ...etc.

- Expand the portfolio feature to have a row for every stock symbol, and the
  user could enter the number of stock they own, and the cost for each, then
  loss/gain can be calculated.

2005-05-10:

- With portfolio, allow the ability to send email alerts when certain thresholds
  are found (e.g. above or below a dollar value or a percentage)
