
Copyright 2004-2005 http://2bits.com

Description:
------------
        
This is a stock module that provides visitors to your site stock quotes
from various market exchanges.

Users can configure a portfolio that would be displayed in a block with
they own stock symbols.

It relies on Yahoo! Finance for getting the market data.

Features:
---------

This module provides several options that customize its look and feel:

* An overview section on the top of the page can contain any text you want
* Can be configured as a block, providing a custom portfolio for logged in
  users

Dependancies:
-------------

This module requires the stockapi module to be installed and configured.

Installation:
-------------

Please see the INSTALL document for details.

Bugs/Features/Patches:
----------------------

If you want to report bugs, feature requests, or submit a patch, please do so
at the project page on the Drupal web site.
http://drupal.org/project/stock

Author
------

Khalid Baheyeldin (http://baheyeldin.com/khalid and http://2bits.com)

If you use this module, find it useful, and want to send the author
a thank you note, then use the Feedback/Contact page at the URL above.

The author can also be contacted for paid customizations of this
and other modules.
